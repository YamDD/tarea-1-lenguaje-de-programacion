Algoritmo EJERCICO15CalcularTiempoConIf
	
		Definir dias, a�os, meses, semanas, dias_restantes Como Entero

		Escribir "Ingrese el n�mero de d�as:"
		Leer dias
		a�os <- 0
		meses <- 0
		semanas <- 0
		Si dias >= 365 Entonces
			a�os <- dias / 365
			dias <- dias - (a�os * 365)
		FinSi

		Si dias >= 30 Entonces
			meses <- dias / 30
			dias <- dias - (meses * 30)
		FinSi
		Si dias >= 7 Entonces
			semanas <- dias / 7
			dias <- dias - (semanas * 7)
		FinSi
		dias_restantes <- dias
		Escribir "El n�mero de d�as ingresado corresponde a:"
		Escribir "A�os: ", a�os
		Escribir "Meses: ", meses
		Escribir "Semanas: ", semanas
		Escribir "D�as: ", dias_restantes
FinAlgoritmo
