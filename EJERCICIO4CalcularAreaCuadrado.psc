//Manejo de datos de entrada: No se ha verificado si las longitudes ingresadas por el usuario son n�meros positivos o si cumplen con las condiciones de un tri�ngulo rect�ngulo (es decir, si ambas longitudes de los catetos son mayores que cero).

//Manejo de resultados imaginarios: Si la suma de los cuadrados de los catetos resulta en un n�mero negativo (lo que puede suceder si los valores de entrada no son v�lidos para formar un tri�ngulo rect�ngulo), entonces el resultado de la ra�z cuadrada ser�a imaginario. En este caso, deber�amos proporcionar un mensaje de error apropiado para informar al usuario que las longitudes de los catetos no forman un tri�ngulo rect�ngulo v�lido.

//Formateo de salida: La longitud de la hipotenusa puede ser un n�mero real con varios decimales. Ser�a �til redondear este valor a una cantidad razonable de decimales o, si se prefiere, expresarlo como un n�mero entero.

Algoritmo EJERCICIO4CalcularAreaCuadrado
	
		Definir lado, area Como Real
		
		Escribir "Ingrese la longitud del lado del cuadrado: "
		Leer lado
		
		area <- lado * lado
		
		Escribir "El �rea del cuadrado es: ", area
FinAlgoritmo

	

