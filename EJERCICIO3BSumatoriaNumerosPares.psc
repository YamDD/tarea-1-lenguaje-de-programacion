Algoritmo EJERCICIO3BSumatoriaNumerosPares
	
	Definir suma, numero Como Entero
	
	suma <- 0
	
	Escribir "Los n�meros pares entre 1 y 100 son:"
	
	Para numero <- 2 Hasta 100 Con Paso 2 Hacer
		Escribir numero
		suma <- suma + numero
	Fin Para
	
	Escribir "La sumatoria de los n�meros pares es: ", suma
FinAlgoritmo