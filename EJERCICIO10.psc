Algoritmo EJERCICIO10
    Definir cantidad_entradas, costo_entrada, total_pagar como Entero
    Definir descuento como Real
    
    Escribir "Ingrese la cantidad de entradas a comprar (m�ximo 4):"
    Leer cantidad_entradas
    
    Segun cantidad_entradas
			Caso 1:
            costo_entrada <- 1000  
			Caso 2:
            costo_entrada <- 1000 * 2
            descuento <- costo_entrada * 0.10  
            costo_entrada <- costo_entrada - descuento
			Caso 3:
            costo_entrada <- 1000 * 3
            descuento <- costo_entrada * 0.15  
            costo_entrada <- costo_entrada - descuento
			Caso 4:
            costo_entrada <- 1000 * 4
            descuento <- costo_entrada * 0.20  
            costo_entrada <- costo_entrada - descuento
			De Otro Modo
            Escribir "Cantidad de entradas no v�lida. Debe ser entre 1 y 4."
    FinSegun
    
    Si cantidad_entradas >= 1 y cantidad_entradas <= 4 Entonces
        Escribir "El total a pagar es:", costo_entrada, "Soles."
    FinSi
FinAlgoritmo

