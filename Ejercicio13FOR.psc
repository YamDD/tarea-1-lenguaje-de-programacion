Algoritmo Ejercicio13FOR 
		Definir obreros, salario_hora, horas_trabajadas, salario_total Como Entero
		Definir costo_hora Como Real
		costo_hora <- 30000
	
		salario_total <- 0
		Escribir "Ingrese el n�mero de obreros:"
		Leer obreros
		
		Si obreros <= 0 Entonces
			Escribir "El n�mero de obreros debe ser mayor que cero."
		Sino
			Para i <- 1 Hasta obreros Hacer
				
				Escribir "Ingrese las horas trabajadas para el obrero ", i, ":"
				Leer horas_trabajadas
				salario_total <- salario_total + (horas_trabajadas * costo_hora)
			FinPara
			Escribir "La n�mina total a pagar es de ", salario_total, " Soles."
		FinSi
FinAlgoritmo
