Algoritmo Ejercicio3SumatoriaMultiplosDeCinco
		Definir suma, numero Como Entero
		
		suma <- 0
		
		Escribir "Los n�meros m�ltiplos de 5 entre 1 y 100 son:"
		
		Para numero <- 5 Hasta 100 Con Paso 5 Hacer
			Escribir numero
			suma <- suma + numero
		Fin Para
		
		Escribir "La sumatoria de los m�ltiplos de 5 es: ", suma
FinAlgoritmo






