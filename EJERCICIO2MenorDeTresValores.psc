//Este algoritmo no es la soluci�n perfecta al ejercicio anterior porque solo comprueba si los valores ingresados 
//son distintos despu�s de haberlos le�do todos. Esto significa que si los primeros dos valores son iguales y el 
//tercero es diferente, el algoritmo no detectar� el problema. Por lo tanto, no garantiza que los tres valores sean 
//distintos antes de continuar con la comparaci�n de mayor y menor.


//Para mejorar este algoritmo, deber�amos verificar la igualdad de los valores a medida que los leemos, en lugar de esperar hasta despu�s de haberlos le�do todos. Si detectamos que dos valores son iguales, debemos mostrar un mensaje de error y detener la ejecuci�n del algoritmo. Por lo tanto, el cambio principal ser�a agregar esta verificaci�n despu�s de leer cada valor.

Algoritmo EJERCICIO2MenorDeTresValores

		Definir A, B, C, menor Como Entero
		
		Escribir "Ingrese el primer valor (A): "
		Leer A
		Escribir "Ingrese el segundo valor (B): "
		Leer B
		Escribir "Ingrese el tercer valor (C): "
		Leer C
		
		Si A < B Y A < C Entonces
			menor = A
		Sino 
			Si B < A Y B < C Entonces
				menor = B
			Sino
				menor = C
			FinSi
			
			Escribir "El menor valor es: ", menor
FinAlgoritmo

	
	

