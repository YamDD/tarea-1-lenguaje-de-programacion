Algoritmo Ejercicio14
		Definir precio, cantidad, total, pago, cambio Como Real
		total <- 0
		Escribir "Ingrese el precio del producto (0 para finalizar):"
		Leer precio
		
		Si precio > 0 Entonces
			Repetir
				Escribir "Ingrese la cantidad del producto:"
				Leer cantidad
				
				subtotal <- precio * cantidad
				total <- total + subtotal
				Escribir "Ingrese el precio del siguiente producto (0 para finalizar):"
				Leer precio
			Hasta Que precio <= 0
		FinSi
		Escribir "El total de la compra es: ", total
		Escribir "Ingrese la cantidad con la que pagar�:"
		Leer pago
		
		Si pago >= total Entonces
			cambio <- pago - total
			Escribir "El cambio es: ", cambio
		Sino
			Escribir "El pago no es suficiente."
		FinSi
FinAlgoritmo
