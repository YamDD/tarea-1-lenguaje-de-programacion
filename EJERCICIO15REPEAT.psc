Algoritmo EJERCICIO15REPEAT
		Definir dias, a�os, meses, semanas, dias_restantes Como Entero
		
		Escribir "Ingrese el n�mero de d�as:"
		Leer dias
		a�os <- 0
		meses <- 0
		semanas <- 0
		Repetir
			Si dias >= 365 Entonces
				a�os <- a�os + 1
				dias <- dias - 365
			FinSi
		Hasta Que dias < 365
		Repetir
			Si dias >= 30 Entonces
				meses <- meses + 1
				dias <- dias - 30
			FinSi
		Hasta Que dias < 30
		Repetir
			Si dias >= 7 Entonces
				semanas <- semanas + 1
				dias <- dias - 7
			FinSi
		Hasta Que dias < 7
		
		dias_restantes <- dias
		Escribir "El n�mero de d�as ingresado corresponde a:"
		Escribir "A�os: ", a�os
		Escribir "Meses: ", meses
		Escribir "Semanas: ", semanas
		Escribir "D�as: ", dias_restantes
FinAlgoritmo
