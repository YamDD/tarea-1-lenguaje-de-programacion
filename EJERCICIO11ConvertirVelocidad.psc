
Algoritmo EJERCICIO11ConvertirVelocidad
    
    Definir velocidad_mps, velocidad_kmh Como Real
	
    Escribir "Ingrese la velocidad en metros por segundo (m/s):"
    Leer velocidad_mps
    velocidad_kmh <- velocidad_mps * (3600 / 1000)
    Escribir "La velocidad es de ", velocidad_kmh, " kilómetros por hora (km/h)."
FinAlgoritmo



